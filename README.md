# Game:

You can move your unit with the keyboard (arrows, asdw or qsdz keys) and shoot
with a left-click of the mouse.  
The more you hit one color, the more this color will attack you while the
other color will stop to attack you.  

"Ob portum" is latin, and means "to the harbor". It is the etymology of the word
"opportune", itself the origin of "opportunistic".

# Dependencies:

* Python 2.7 (http://www.python.org/)
* PyGame 1.9 (http://www.pygame.org/)

# Run the game:

python run_game.py  
(run_game.pyw exists for Windows and Mac OS X, but it was not tested)

# Licence:

Code:  
ObPortum is under GPL3 licence.  
Cf. COPYING

Font:  
The font Play is under OFL licence.  
https://fonts.google.com/specimen/Play

Graphics:  
created for the game and are under CC BY 3.0

Sounds:  
laser.ogg (CC BY 3.0 TijsWijnen
https://www.freesound.org/people/TiesWijnen/sounds/339040/)  
hit.ogg (CC BY 3.0 n_audioman 
https://www.freesound.org/people/n_audioman/sounds/273562/)  
button_cl.ogg (CC0 1.0 n_audioman
https://www.freesound.org/people/n_audioman/sounds/275897/)  
button_ov.ogg (CC0 1.0 n_audioman
https://www.freesound.org/people/n_audioman/sounds/266742/)  
achievement.ogg (CC BY 3.0 n_audioman 
https://www.freesound.org/people/n_audioman/sounds/266738/)  
death.ogg (CC0 1.0 n_audioman
https://www.freesound.org/people/n_audioman/sounds/320441/)  

Music:  
renamed and converted to ogg  
Oortsche Wolke (ID 80) by Lobo Loco  
CC BY NC ND 4.0  
http://freemusicarchive.org/music/Lobo_Loco/My_Scifi_Sofa_Visions/Oortsche_Wolke_ID_80  
