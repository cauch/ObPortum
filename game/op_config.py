#! /usr/bin/env python

#    This file is part of ObPortum.
#
#    ObPortum is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ObPortum is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ObPortum.  If not, see <http://www.gnu.org/licenses/>.

import os
import time

class Config:
    def __init__(self,core):
        """
        Basic tool to deal with the config
        """
        self.core = core
        self.difficulty = "normal"   #difficulty level
        self.color = "whiterandom"   #color scheme for the units
        self.sound1 = 2              #effect sound volume
        self.sound2 = 1              #music sound volume
        self.fullscreen = 0          #fullscreen
        self.achiev_done = {}         #achievement obtained
        self.highscores = {}         #high scores
        self.plots = 1               #History will store info to do plots
        self.pausewhenout = 1        #switch to pause when the window lose the focus
        #difficulty parameters
        self.dif = {}   
        self.dif["start_health"] = 0    #health at start
        self.dif["start_ammo"] = 0      #ammo at start
        self.dif["bonus_health"] = 0    #health from bonus
        self.dif["bonus_ammo"] = 0      #ammo from bonus
        self.dif["bonus_ratio"] = 3     #ratio from bonus
        self.dif["generation_unit"] = 0.75#delay (in seconds) for generating units
        self.dif["generation_bonus"] = 10 #delay (in seconds) for generating bonus
        self.dif["ai_bulletprob"] = 60  #number used when deciding if it shoot
        self.dif["ai_playerprob"] = 1   #factor increasing the prob to shoot to player (higher = harder)
        self.dif["ratio_bullet"] = 0.5  #bullet power = this power of negative ratio 
        #
        self.firsttime = 0#check if it's the first time (save.cfg is not there)
        self.load()

    def print_dif(self):
        """
        Return correct english description of the difficulty level
        """
        if self.difficulty == "3teams":
            return "3 teams game"
        if self.difficulty == "6teams":
            return "6 teams game"
        return self.difficulty

    def print_fs(self):
        """
        Return correct english description of the fullscreen status
        """
        if self.fullscreen:
            return "on"
        return "off"

    def print_plo(self):
        """
        Return correct english description of the plot functionality status
        """
        if self.plots:
            return "enabled"
        return "disabled"

    def print_pwo(self):
        """
        Return correct english description of the pause-when-out status
        """
        if self.pausewhenout:
            return "enabled"
        return "disabled"

    def print_col(self):
        """
        Return correct english description of the color scheme
        """
        if self.color == "whiterandom":
            return "white and random"
        elif self.color == "random":
            return "random"
        elif self.color == "whitebluered":
            return "white, blue and red"
        else:
            return "custom"

    def print_sound(self,i):
        """
        Return correct english description of the volume
        argument:
          i: =1 for effect sounds, =2 for music
        """
        sound = 0
        if i == 1:
            sound = self.sound1
        if i == 2:
            sound = self.sound2
        val = ["off","#____","##___","###__","####_","#####"]
        return val[sound]

    def get_sound1(self):
        """
        Return effect volume factors to apply based on the option value
        """
        vol = [0,0.02,0.1,0.25,0.5,1]
        return vol[self.sound1]

    def get_sound2(self):
        """
        Return music volume factors to apply based on the option value
        """
        vol = [0,0.02,0.1,0.25,0.5,1]
        return vol[self.sound2]

    def rotate(self,text):
        """
        Switch to the next possible option.
        argument:
          text: code for which options is changed
        """
        if text == "dif":
            rv = ["normal","difficult","hard","3teams","6teams","easy"]
            i = rv.index(self.difficulty)
            self.difficulty = rv[(i+1)%len(rv)]
        elif text == "col":
            rv = ["whiterandom","random","whitebluered"]
            if self.color not in rv:
                rv.append(self.color)
            i = rv.index(self.color)
            self.color = rv[(i+1)%len(rv)]
        elif text == "so1":
            self.sound1 += 1
            if self.sound1 > 5:
                self.sound1 = 0
        elif text == "so2":
            self.sound2 += 1
            if self.sound2 > 5:
                self.sound2 = 0
        elif text == "fsc":
            self.fullscreen = (self.fullscreen+1)%2
        elif text == "plo":
            self.plots = (self.plots+1)%2
        elif text == "pwo":
            self.pausewhenout = (self.pausewhenout+1)%2
        self.apply_option(text)

    def apply_option(self,text):
        """
        Apply the option
        argument:
          text: code for which options is changed
        """
        if text == "dif":
            #update the open achiev
            self.core.achiev_open = []
            for k in self.core.achiev:
                if not self.is_achieved(k):
                    self.core.achiev_open.append(k)
            #change self.dif
            self.apply_dif()
        elif text == "col":
            pass
        elif text == "so1":
            self.core.display.change_volume_sound()
        elif text == "so2":
            self.core.display.change_volume_music()
        elif text == "fsc":
            self.core.display.set_fullscreen(self.fullscreen)
        elif text == "plo":
            pass
        elif text == "pwo":
            pass
        elif text == "all":
            for t in ["dif","col","so1","so2","fsc","pwo"]:
                self.apply_option(t)

    def apply_dif(self):
        """
        A separated function to apply the difficulty level
        """
        self.core.nside = 2
        if self.difficulty == "normal" or \
           self.difficulty == "3teams" or \
           self.difficulty == "6teams":
            self.dif["start_health"] = 100
            self.dif["start_ammo"] = 100
            self.dif["bonus_health"] = 25
            self.dif["bonus_ammo"] = 25
            self.dif["bonus_ratio"] = 3
            self.dif["generation_unit"] = 0.85
            self.dif["generation_bonus"] = 10
            self.dif["ai_bulletprob"] = 80
            self.dif["ai_playerprob"] = 1.0
            self.dif["ratio_bullet"] = 0.5
        elif self.difficulty == "difficult":
            self.dif["start_health"] = 100
            self.dif["start_ammo"] = 100
            self.dif["bonus_health"] = 20
            self.dif["bonus_ammo"] = 25
            self.dif["bonus_ratio"] = 3
            self.dif["generation_unit"] = 0.75
            self.dif["generation_bonus"] = 10
            self.dif["ai_bulletprob"] = 60
            self.dif["ai_playerprob"] = 1.0
            self.dif["ratio_bullet"] = 0.5
        elif self.difficulty == "hard":
            self.dif["start_health"] = 100
            self.dif["start_ammo"] = 100
            self.dif["bonus_health"] = 20
            self.dif["bonus_ammo"] = 25
            self.dif["bonus_ratio"] = 3
            self.dif["generation_unit"] = 0.7
            self.dif["generation_bonus"] = 10
            self.dif["ai_bulletprob"] = 60
            self.dif["ai_playerprob"] = 1.2
            self.dif["ratio_bullet"] = 0.75
        elif self.difficulty == "easy":
            self.dif["start_health"] = 100
            self.dif["start_ammo"] = 100
            self.dif["bonus_health"] = 30
            self.dif["bonus_ammo"] = 50
            self.dif["bonus_ratio"] = 3
            self.dif["generation_unit"] = 1
            self.dif["generation_bonus"] = 10
            self.dif["ai_bulletprob"] = 80
            self.dif["ai_playerprob"] = 0.8
            self.dif["ratio_bullet"] = 0.5
        if self.difficulty == "3teams":
            self.core.nside = 3
        if self.difficulty == "6teams":
            self.core.nside = 6

    def load(self):
        """
        Load the save.cfg file, read it and fill the variables accordingly
        """
        filename = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                                "..","save.cfg")
        if os.path.exists(filename):
            fi = open(filename,"r")
            da = fi.readlines()
            for d in da:
                t = d.strip()
                #get rid of empty lines or comments
                if not t or t.startswith("#"):
                    continue
                t = t.split("#")[0]
                #fill the variables
                if t.startswith("dif"):
                    self.difficulty = t.split()[1]
                elif t.startswith("col"):
                    self.color = t.split()[1]
                elif t.startswith("so1"):
                    self.sound1 = int(t.split()[1])
                elif t.startswith("so2"):
                    self.sound2 = int(t.split()[1])
                elif t.startswith("fsc"):
                    self.fullscreen = int(t.split()[1])
                elif t.startswith("plo"):
                    self.plots = int(t.split()[1])
                elif t.startswith("pwo"):
                    self.pausewhenout = int(t.split()[1])
                elif t.startswith("a"):
                    k = t[1:].split()[0]
                    self.achiev_done[k] = t.split()[1].split(",")
                elif t.startswith("h"):
                    k = t[1:].split()[0]
                    self.highscores[k] = t.split()[1].split(",")
        else:
            self.firsttime = 1

    def save(self):
        """
        Save the config into the save.cfg file
        """
        filename = os.path.join(os.path.abspath(os.path.dirname(__file__)), 
                                "..","save.cfg")
        fi = open(filename,"w")
        fi.write("%s %s\n"%("dif",self.difficulty))
        fi.write("%s %s\n"%("col",self.color))
        fi.write("%s %i\n"%("so1",self.sound1))
        fi.write("%s %i\n"%("so2",self.sound2))
        fi.write("%s %i\n"%("fsc",self.fullscreen))
        fi.write("%s %i\n"%("plo",self.plots))
        fi.write("%s %i\n"%("pwo",self.pausewhenout))
        for k in self.achiev_done:
            if self.achiev_done[k]:
                fi.write("a%s %s\n"%(k,",".join(self.achiev_done[k])))
        for k in self.highscores:
            if self.highscores[k]:
                fi.write("h%s %s\n"%(k,",".join(self.highscores[k])))

    def get_highscores(self):
        """
        Return a list of the highscore for the current difficulty level
        """
        if self.difficulty not in self.highscores.keys():
            self.highscores[self.difficulty] = []
        hs = self.highscores[self.difficulty][:]
        for i in range(5-len(hs)):
            hs.append("---")
        return hs

    def get_highscores_time(self):
        """
        Return the second of the highest score for the current difficulty level
        """
        if self.difficulty not in self.highscores.keys():
            self.highscores[self.difficulty] = []
        x = "00:00:00"
        if self.highscores[self.difficulty]:
            x = self.highscores[self.difficulty][0]
        ti = (int(x.split(":")[0])*60*60) + \
             (int(x.split(":")[1])*60) + \
             int(x.split(":")[2].split("_")[0])
        return ti

    def add_highscores(self):
        """
        Called by Core when the game is finished, to had the current time in the
        highscores.
        It is stored on the form: HH:MM:SS_DD/MM/YY_HH:MM
        """
        if self.difficulty not in self.highscores.keys():
            self.highscores[self.difficulty] = []
        hs = [[(int(x.split(":")[0])*60*60)+(int(x.split(":")[1])*60)+int(x.split(":")[2].split("_")[0]),x]
                   for x in self.highscores[self.difficulty]]
        ct = time.strftime("%d/%m/%y_%H:%M",time.localtime())
        ne = [int(self.core.player_time*1./self.core.fps),
              "%s_%s"%(self.core.get_player_time(),ct)]
        hs.append(ne)
        hs.sort(reverse=True)
        hs = hs[:5]
        if ne in hs:
            self.highscores[self.difficulty] = [x[1] for x in hs]
            self.save()

    def add_achiev(self,an):
        """
        A achievement has been done, add it to the list and save
        argument:
          an: name of the achievement
        """
        if self.difficulty not in self.achiev_done.keys():
            self.achiev_done[self.difficulty] = []
        if an not in self.achiev_done[self.difficulty]:
            self.achiev_done[self.difficulty].append(an)
        self.save()

    def is_achieved(self,an):
        """
        Return True if the achievement has been achieved
        """
        if self.difficulty not in self.achiev_done.keys():
            self.achiev_done[self.difficulty] = []        
        if an in self.achiev_done[self.difficulty]:
            return 1
        return 0
