#! /usr/bin/env python

#    This file is part of ObPortum.
#
#    ObPortum is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ObPortum is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ObPortum.  If not, see <http://www.gnu.org/licenses/>.

class History:
    def __init__(self,core):
        """
        Keep history of a game.
        Used for plots.
        """
        self.core = core
        self.h = {}
        self.t = []

    def reset(self):
        """
        Reset the variables
        """
        self.h = {}
        self.t = []

    def add(self,event,arg=None):
        """
        Add an event to the list
        self.h is a dico of time containing array of [event,arg]
        self.t is the sorted list of h dico keys
        arguments:
          event: a text code, such as "gi", "h0", "m0", ...
          arg: possible other info
        """
        #if the functionality is turned off, stop there
        if self.core.config.plots == 0:
            return
        #if the player is dead, stop here
        if not self.core.units[0]:
            return
        #add in self.h and self.t
        time = self.core.time
        if not self.t or self.t[0] < time:
            self.t = [time]+self.t
            self.h[time] = []
        self.h[time].append([event,arg])

    def plot(self,subject):
        """
        Create a (x,y) array for a plot
        argument:
          subject: code text about what to plot
        return:
          array of (x,y) position + double if ymin and ymax
        x is going from 0 up to 1
        """
        if not self.t:
            return []
        #dx to normalize x-axis to 0->1
        start,stop = self.t[-1],self.t[0]
        if start == stop:
            return []
        dx = 1./(stop-start)
        #initialize variables
        values = []
        ymax,ymin = None,None
        var1 = []
        var2,var3 = 0,0
        #loop on all entries
        for i in range(len(self.t)-1,-1,-1):
            x = (self.t[i]-start)*dx
            y = None
            if subject.startswith("accu") or subject.startswith("acsw"):
                #bullet accuracy
                side = subject[4:]
                r = self.get_elements_from_evt(self.t[i],"m"+side)
                if r:
                    var1.append(0)
                    var2 += 1
                r = self.get_elements_from_evt(self.t[i],"h"+side)
                if r:
                    var1.append(1)
                    var1.append(1)
                    var3 += 1
                if len(var1) > 20:
                    var1 = var1[-20:]
                if subject.startswith("acsw"):
                    if len(var1):
                        y = sum(var1)*1./len(var1)
                    else:
                        y = 0
                else:
                    if var2 != 0:
                        y = var3*1./var2
                    else:
                        y = 0
            if subject.startswith("rkh"):
                #ratio killed / being_hit
                side = int(subject[3:])
                r1 = self.get_elements_from_evt(self.t[i],"h0")
                for r in r1:
                    for rr in r:
                        if rr == side:
                            var2 += 1
                r2 = self.get_elements_from_evt(self.t[i],"0h")
                for r in r2:
                    for rr in r:
                        if rr == side:
                            var3 += 1
                if var3 != 0:
                    y = var2*1./var3
                else:
                    y = 0
            if subject.startswith("hdp"):
                #hits done by player
                side = int(subject[3:])
                r1 = self.get_elements_from_evt(self.t[i],"h0")
                for r in r1:
                    for rr in r:
                        if rr == side:
                            var2 += 1
                y = var2
            if subject.startswith("hrp"):
                #hits received by player
                side = int(subject[3:])
                r1 = self.get_elements_from_evt(self.t[i],"0h")
                for r in r1:
                    for rr in r:
                        if rr == side:
                            var2 += 1
                y = var2
            if subject == "health":
                #health
                r = self.get_elements_from_evt(self.t[i],"gi")
                if r:
                    y = r[-1][0][-2]
            if subject == "ammo":
                #ammo
                r = self.get_elements_from_evt(self.t[i],"gi")
                if r:
                    y = r[-1][0][-1]
            if subject.startswith("en"):
                #enemy diplomacy level or number
                ii = int(subject[2])*2
                if subject[3] == "r":
                    ii += 1
                r = self.get_elements_from_evt(self.t[i],"gi")
                if r:
                    y = r[-1][0][ii]
            if y != None:
                if ymax == None or y > ymax:
                    ymax = y
                if ymin == None or y < ymin:
                    ymin = y
                values.append([x,y])
        return values,[ymin,ymax]

    def get_elements_from_evt(self,t,event):
        """
        For a key 't', self.h[t] can have several events
        This fct return the element of the event 'event' for self.h[t]
        return:
          array [event, [args]]
        """
        #event can be a string or a vector of string
        if type(event) == type(""):
            event = [event]
        res = []
        for e in self.h[t]:
            if e[0] in event:
                res.append(e[1:])
        return res
                    
