#! /usr/bin/env python

#    This file is part of ObPortum.
#
#    ObPortum is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ObPortum is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ObPortum.  If not, see <http://www.gnu.org/licenses/>.

from random import randint,random
from math import sqrt

class AI_basic:
    def __init__(self,core):
        """
        Basic algo for the units to move and shoot
        """
        self.core = core

    def think(self,u):
        """
        Core will call this function regularly
        arguments:
          u: unit
        """
        side = u.side
        if not u.brain.get("direction"):
            u.brain["direction"] = [randint(-1,1),randint(-1,1)]
        if randint(0,5)==0:
            #randomly change direction
            u.brain["direction"] = [randint(-1,1),randint(-1,1)]
            #but prefer to stay in the screen
            if u.pos[0] < 10:
                u.brain["direction"][0] = randint(0,1)
            if u.pos[1] < 10:
                u.brain["direction"][1] = randint(0,1)
            if u.pos[0] > self.core.display.screen_width-10-self.core.display.unit_size:
                u.brain["direction"][0] = randint(-1,0)
            if u.pos[1] > self.core.display.screen_height-10-self.core.display.unit_size:
                u.brain["direction"][1] = randint(-1,0)
        #move
        u.move(u.brain["direction"])
        #randomly shoot
        prob = self.core.config.dif["ai_bulletprob"]

        # the more the player play, the more it becomes fast
        # 6 minutes = - ~10
        correction = (self.core.player_time*self.core.fps/50000.)
        prob = max(5,int(round(prob-correction)))
        if randint(0,prob) == 0:

            playerfactor = self.core.config.dif["ai_playerprob"]
            real_side = 0
            if u.side == 0:
                playerfactor = 0
                side = randint(1,self.core.nside)
            else:
                real_side = side

            chosen_side = -1
            #1) get all the enemy units
            enemy_list = []
            for ll in self.core.units.keys():
                if ll == 0 or ll == side:
                    continue
                enemy_list += [ll]*len(self.core.units[ll])
            #2) if there is no enemy
            if len(enemy_list) == 0:
                threshold = 0
                if self.core.ratio[side] < -5:
                    #2.1) if the player is enemy, choose it
                    threshold = 1
                elif self.core.ratio[side] < 5:
                    #2.2) if the player is neutral, between 0.2 and 0.6 chance to be picked
                    threshold = 0.2+(0.04*(5-self.core.ratio[side]))
                else:
                    #2.3) if he player is ally, 0.1 chance to be picked
                    threshold = 0.1
                if random()<threshold*playerfactor:
                    chosen_side = 0
            else:
                #3) if there are enemies
                factor = 1
                if self.core.ratio[side] > 5:
                    #3.1) if the player is friendly, it has 10%
                    #   of chance to be chosen w.r.t. an enemy
                    #   (if the enemy has 50% chance, it has 5% chance)
                    factor = 0.1
                elif self.core.ratio[side] > -5:
                    #3.2) if the player is neutral, it has between 0.5
                    #     and 1.5 more chance than an enemy
                    factor = 0.5 + ((5-self.core.ratio[side])*0.1)
                else:
                    #3.3) if the player is enemy, it has between 2 and ...
                    #     more chance to be picked
                    factor = 2 + ((self.core.ratio[side]+5)*-0.5)
                proba_enemy = 1./len(enemy_list)
                proba_player = proba_enemy*factor*playerfactor
                if random() < proba_player:
                    chosen_side = 0
                else:
                    chosen_side = enemy_list[randint(0,len(enemy_list)-1)]
            #apply the decision
            if chosen_side == -1:
                return
            if self.core.units[chosen_side]:
                enemy_unit = self.core.units[chosen_side][randint(0,len(self.core.units[chosen_side])-1)]
                self.core.shoot(real_side,
                                self.core.units[real_side].index(u),
                                #[enemy_unit.pos[x]+(self.core.display.unit_size/2) for x in range(2)])
                                [enemy_unit.pos[x]+(self.core.display.unit_size/2)+(10*enemy_unit.brain.get("direction",[0,0])[x]) for x in range(2)])

