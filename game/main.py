#    This file is part of ObPortum.
#
#    ObPortum is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ObPortum is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ObPortum.  If not, see <http://www.gnu.org/licenses/>.

import op_core
import op_display

def main():
    core = op_core.Core()
    display = op_display.Display()
    core.display = display
    display.core = core
    core.load()

