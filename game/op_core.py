#! /usr/bin/env python

#    This file is part of ObPortum.
#
#    ObPortum is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ObPortum is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ObPortum.  If not, see <http://www.gnu.org/licenses/>.

from random import randint
from math import sqrt
from op_ai import *
from op_config import *
from op_history import *
import op_achievements 

class Unit:
    def __init__(self,core):
        """
        Units (if I have time, I will change it such that it's inherited from PGUnit)
        """
        self.core = core
        self.what = 0        # =0 -> it's an unit
        self.pgunit = None   #the corresponding sprite
        self.pos = [390,290] #top-left px position on the screen
        self.side = 0        #side, 0=player
        self.brain = {}      #useful things to keep for the AI

    def move(self,direction):
        """
        Use this function to move the object.
        argument:
          direction: (x,y) corresponding to the step
        """
        speed = 5.      
        self.pos[0] += direction[0]*speed
        self.pos[1] += direction[1]*speed

class Bullet:
    def __init__(self,core):
        """
        Bullets
        """
        self.core = core
        self.what = 1        # =1 -> it's an bullet
        self.pgunit = None   #the corresponding sprite
        self.pos = [390,290] #top-left px position on the screen
        self.direction = [0,1] #(x,y) step 
        self.side = 0        #side of the unit that created the bullet, 0=player
        self.time = 80       #time of life

    def step(self):
        """
        Call this function to make the bullet progress
        return 0 if the object should be deleted
        """
        self.pos[0] += self.direction[0]
        self.pos[1] += self.direction[1]
        self.time += -1
        if self.time < 0:
            return 0
        return 1

class Core:
    def __init__(self):
        """
        Main algorithms and structures.
        Independant of pygame.
        """
        self.display = None
        self.play = 1       #0:quit, 1:play
        self.units = {}     #units[0] contains [player]
                            # units[1] contains [team1_unit1,team1_unit2,...]
                            # units[2] contains [team2_unit1,team_unit2,...]
        self.bullets = []   #list of currently moving bullets
        self.ratio = {}     #ratio[X] is the float corresponding to the level 
                            # of friendship of the teamX
        self.lasthelp = {}  #keep track of last time you helped the side X
        self.nside = 2      #number of sides
        #initialize the dicos
        self.units[0] = []
        for i in range(self.nside):
            self.units[i+1] = []
            self.lasthelp[i+1] = 0
            self.ratio[i+1] = 0
        self.ai = AI_basic(self)   #AI tool
        self.config = Config(self) #Config tool
        self.h = History(self)     #History tool

        self.achiev = {}      #total list of achievements by names
        self.achiev_open = [] #name of the achievs still open

        self.next_fct = None #fct that is called after run (if None, quit)
        self.current_hs = 0  #current highscore time, to announce when the player beats it

        self.time = 0        #time in iteration of the while loop
        self.player_time = 0 #time in iteration, during the life of the player
        self.fps = 40        #fps used in display -> 1 iteration = 1./fps seconds
        self.cnt_g_unit = 0  #counter for unit generation
        self.cnt_g_bonus = 0 #counter for bonus generation

    def load(self):
        """
        First function called
        """
        #initialize Display
        self.display.init()
        #find all the achievements
        for l in dir(op_achievements):
            if l.startswith("A_"):
                self.achiev[l[2:]] = eval("op_achievements.%s(self)"%l)
        self.achiev_open = []
        for k in self.achiev:
            if not self.config.is_achieved(k):
                self.achiev_open.append(k)
        #start by displaying the menu
        self.display.mode = "menu"
        if self.config.firsttime:
            self.config.firsttime = 0
            self.display.menu.where = "firsttime"
        #create background actions
        for side in [1,2]:
            for i in range(5):
                p = Unit(self)
                x = randint(0,self.display.screen_width)
                y = randint(0,self.display.screen_height)
                p.pos = [x,y]
                p.side = side
                self.display.associate_unit(p)
                self.units[side].append(p)
        #main loop
        self.run()

    def run(self):
        """
        Main loop
        """
        while self.play:
            self.display.update()
            if self.display.mode != "pause":
                self.time += 1
                self.update()
        if self.next_fct:
            self.next_fct()
        else:
            self.display.end()

    def update(self):
        """
        All the update to do when the game is playing
        """
        #fill History with global info ('gi')
        if self.time%(self.fps/2) == 0 and self.units[0]:
            info = []
            for i in range(self.nside):
                info.append(len(self.units[i+1]))
                info.append(self.ratio[i+1])
            info.append(self.units[0][0].health)
            info.append(self.units[0][0].ammo)
            self.h.add("gi",info)
        #keep track of the player time
        if self.units[0]:
            self.player_time = self.time
        #friendship slowly disappear
        if self.time%self.fps == 0:
            self.change_ratio_lasthelp()
        #create new units
        self.generate_unit()
        #create new bonus
        if self.display.mode == "game":
            self.generate_bonus()
        #check the achievements
        if self.display.mode == "game":
            self.check_achiev()
        #check if the high score is beaten
        if self.display.mode == "game":
            if self.current_hs >= 0:
                if self.player_time*1./self.fps > self.current_hs:
                    self.display.play_sound("achievement")
                    self.display.message("New best high score!")
                    self.current_hs = -1
        #call the AI of the units
        for k in self.units:
            if k == 0:
                continue
            for u in self.units[k]:
                self.ai.think(u)

    def new_game(self):
        """
        Launch a new game.
        It is done by letting the loop finish
        but set 'next_fct' to load a new game
        """
        self.play = 0
        for a in self.achiev_open:
            self.achiev[a].reset()
        self.next_fct = self.load_game() 

    def load_game(self):
        """
        Everything that you should reset before
        starting a new game
        """
        #reset units and bullets
        for k in self.units:
            for u in self.units[k][:]:
                self.kill(u,silent=1)
        for u in self.bullets[:]:
            self.kill(u)
        if self.units[0]:
            self.kill(self.units[0][0],silent=1)
        #reset variables
        self.next_fct = None
        self.display.set_colors()
        self.display.mode = "game"
        self.play = 1
        self.units[0] = []
        for i in range(self.nside):
            self.units[i+1] = []
            self.ratio[i+1] = 0
            self.lasthelp[i+1] = 0
        self.time = 0
        self.player_time = 0
        self.current_hs = self.config.get_highscores_time()
        #reset display and history
        self.display.reset()
        self.h.reset()
        self.h.add("st")
        self.config.apply_dif()
        #create a new player
        player = Unit(self)
        player.health = self.config.dif["start_health"]
        player.ammo = self.config.dif["start_ammo"]
        self.units[0] = [player]
        self.display.associate_unit(player)
        #start the loop
        self.run()

    def check_achiev(self):
        """
        Check achievements
        """
        for a in self.achiev_open[:]:
            r = self.achiev[a].check()
            if r:
                self.display.play_sound("achievement")
                self.config.add_achiev(a)
                self.display.message("Achievement unlocked: %s"%self.achiev[a].title)
                self.achiev_open.remove(a)

    def generate_unit(self):
        """
        Called each iteration of the loop
        Generate new enemy units
        """
        #only do it from time to time
        time = (self.config.dif["generation_unit"])*self.fps
        self.cnt_g_unit += 1
        ## the more the player play, the more it becomes fast
        #correction = (self.player_time*self.fps/300000.)
        #time = max(5,time-correction)
        if self.cnt_g_unit >= time:
            self.cnt_g_unit = 0
        else:
            return
        #what do we want to generate ?
        lk = [0]+self.units.keys()[:]+self.units.keys()[:]
        side = lk[randint(0,len(lk)-1)]
        if side == 0:
            return
        #create the unit
        p = Unit(self)
        pos = randint(0,(self.display.screen_width+self.display.screen_height)*2)
        if pos < self.display.screen_width:            
            p.pos[0] = pos
            p.pos[1] = -1*self.display.unit_size
        elif pos < self.display.screen_width+self.display.screen_width:            
            p.pos[0] = pos-self.display.screen_width
            p.pos[1] = self.display.screen_height+self.display.unit_size
        elif pos < self.display.screen_width+self.display.screen_width+self.display.screen_height:
            p.pos[1] = pos-self.display.screen_width-self.display.screen_width
            p.pos[0] = self.display.screen_width+self.display.unit_size
        else:
            p.pos[1] = pos-self.display.screen_width-self.display.screen_width-self.display.screen_height
            p.pos[0] = -1*self.display.unit_size
        p.side = side
        self.display.associate_unit(p)
        self.units[side].append(p)
        #notify the History
        self.h.add("au",[side,p.pos[0],p.pos[1]])

    def generate_bonus(self):
        """
        Called each iteration of the loop
        Generate new bonuses
        """
        #only do it from time to time
        # the more the player play, the more it becomes rare
        time = (self.config.dif["generation_bonus"])*self.fps
        self.cnt_g_bonus += 1
        correction = (self.player_time*self.fps/10000.)
        time = time+correction
        if self.cnt_g_bonus >= time:
            self.cnt_g_bonus = 0
        else:
            return
        #random position and flavor
        pos = [randint(10,self.display.screen_width-20),randint(10,self.display.screen_height-20)]
        rs = ["r1","r2","r3","r4","li","li","bu","bu","bu","bu"]
        self.display.create_bonus(pos,rs[randint(0,len(rs)-1)])

    def get_bonus(self,r):
        """
        Called when the player collider with a bonus sprite
        argument:
          r: the bonus flavor
        """
        if not self.units[0]:
            return
        self.display.play_sound("button_cl")
        if r == "li":
            self.units[0][0].health += self.config.dif["bonus_health"]
        elif r == "bu":
            self.units[0][0].ammo += self.config.dif["bonus_ammo"]
        elif r in ["r1","r2","r3","r4"]:
            rr = [[self.ratio[k],k] for k in self.ratio.keys()]
            rr.sort()
            if r == "r1":
                self.ratio[rr[-1][1]] += -1*self.config.dif["bonus_ratio"]
            if r == "r2":
                self.ratio[rr[-1][1]] += self.config.dif["bonus_ratio"]
            if r == "r3":
                self.ratio[rr[0][1]] += self.config.dif["bonus_ratio"]
            if r == "r4":
                self.ratio[rr[0][1]] += -1*self.config.dif["bonus_ratio"]

    def kill_bonus(self,b):
        """
        Called when the bonus is deleted
        argument:
          b: the bonus sprite
        """
        pass

    def shoot(self,side,index,target):
        """
        Create a bullet fired to the target
        argument:
          side: the side of the bullet (0:player, 1:team1, ...)
          index: the index integer of the unit firing the bullet
          target: the (x,y) position to where it's shooted
        """
        if side == 0:
            if self.units[side][index].ammo <= 0:
                return
            self.units[side][index].ammo += -1
        self.display.play_sound("laser")
        b = Bullet(self)
        b.pos[0] = self.units[side][index].pos[0]+(self.display.unit_size/2)
        b.pos[1] = self.units[side][index].pos[1]+(self.display.unit_size/2)
        b.side = self.units[side][index].side
        x,y = target[0]-b.pos[0],target[1]-b.pos[1]
        speed = 10.
        if x**2+y**2 != 0:
            x,y = x*speed*1./sqrt(x**2+y**2),y*speed*1./sqrt(x**2+y**2)
        b.direction = [x,y] 
        self.display.associate_bullet(b)
        self.bullets.append(b)

    def kill_player(self,silent=0):
        """
        Called when the player is killed
        """
        if silent == 0:
            self.display.play_sound("death")
            self.display.game_over()
        self.config.add_highscores()

    def kill(self,unit,silent=0):
        """
        Called when an unit is killed (or should be removed)
        """
        self.display.kill(unit.pgunit)
        if unit.what == 0:
            if unit.side == 0:
                self.kill_player(silent)
            if unit in self.units[unit.side]:
                self.units[unit.side].remove(unit)
            else:
                print "trying to remove an unit that does not exist"
        elif unit.what == 1:
            self.h.add("m%i"%unit.side)
            if unit in self.bullets:
                self.bullets.remove(unit)
            else:
                print "trying to remove a bullet that does not exist"
        else:
            print "trying to kill something with wath value",unit.what

    def change_ratio_hit(self,i):
        """
        Change the ratio after an unit has been hit
        argument:
          i: side of the unit that was hit
        """
        for k in self.ratio:
            if k == i:
                continue
            n1 = len(self.units[k])
            n2 = len(self.units[i])
            v = max((n1+15)*1./(n2+15),(n2+15)*1./(n1+15))
            self.ratio[k] += v*(2./self.nside)
            self.lasthelp[k] = int(self.time*1./self.fps)
        self.ratio[i] += -1.2

    def change_ratio_lasthelp(self):
        """
        If you stay too long without helping the team X
        then you start becoming less friendly
        """
        if not self.units[0]:
            return
        for k in self.ratio:
            ti = self.time*1./self.fps
            dt = sqrt((ti-self.lasthelp[k]))
            if self.ratio[k] == max([self.ratio[x] for x in self.ratio]):
                #the first lose more -> slightly decrease the gap
                self.ratio[k] += -0.008*dt
            else:
                self.ratio[k] += -0.006*dt

    def hit(self,hit_unit,bullet):
        """
        Called when a bullet sprite collide with a unit sprite
        argument:
          hit_bullet: Unit of the unit hit by the bullet
          bullet: Unit of the bullet
        """
        if bullet.side == hit_unit.side:
            return
        self.display.play_sound("hit")
        self.h.add("h%i"%bullet.side,hit_unit.side)
        if bullet.side == 0:
            self.change_ratio_hit(hit_unit.side)
        if hit_unit.side == 0:
            self.h.add("0h",bullet.side)
            hit_unit.health += -5
            if self.ratio[bullet.side] < 0:
                power = self.config.dif["ratio_bullet"]
                hit_unit.health += -1*int(pow(self.ratio[bullet.side]*-1/power,power))
            if hit_unit.health <= 0:
                self.kill(hit_unit)                
        else:
            self.kill(hit_unit)
        self.kill(bullet)

    def get_player_time(self):
        """
        Return a proper english text for the time
        """
        #format: "00:00:00"
        ntime = int(self.player_time*1./self.fps)
        second = ntime % 60
        ntime = (ntime - second)/60
        minute = ntime % 60
        ntime = (ntime - minute)/60
        hour = ntime % 24
        tt = "%02i:%02i:%02i"%(hour,minute,second)
        return tt

    def status(self,k):
        """
        Return a proper diplomatic level text
        """
        if self.ratio[k] < -5:
            return "enemy"
        if self.ratio[k] < 5:
            return "neutral"
        return "friend"


