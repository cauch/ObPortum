#! /usr/bin/env python

#    This file is part of ObPortum.
#
#    ObPortum is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ObPortum is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ObPortum.  If not, see <http://www.gnu.org/licenses/>.

#import basic pygame modules
try:
    import pygame
    from pygame.locals import *
except ImportError:
    print("pygame 1.9.1 or higher is required")

import os
import random
from op_menu import Menu
import thread

class PGUnit(pygame.sprite.Sprite):
    def __init__(self,unit,display,what):
        """
        sprite corresponding to an unit (what==0)
        or a bullet (what==1)
        """
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.unit = unit #the corresponding Unit
        self.display = display
        self.what = what 
        unit.pgunit = self
        self.size = [display.unit_size,5][self.what]
        self.image = pygame.Surface((self.size,self.size)).convert_alpha()
        self.cl = display.colors[unit.side]
        self.image.fill(self.cl)
        self.rect = self.image.get_rect()
        self.shadow_i = 0
        self.refresh()

    def refresh(self):
        """
        called by the main display loop
        """
        self.rect.left = int(round(self.unit.pos[0]))
        self.rect.top = int(round(self.unit.pos[1]))
        self.shadow_i += 1
        limit = [3,1][self.what]
        if self.shadow_i > limit:
            self.shadow_i = 0
            self.display.create_shadow(self)

class PGShadow(pygame.sprite.Sprite):
    def __init__(self,pgunit):
        """
        sprite corresponding to a shadow created by an unit or a bullet
        """
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.size = pgunit.size
        self.image = pygame.Surface((self.size,self.size)).convert_alpha()
        self.cl = pgunit.cl
        self.image.fill(self.cl+[100])
        self.rect = self.image.get_rect()
        self.rect.left = pgunit.rect.left
        self.rect.top = pgunit.rect.top
        self.time = 0

    def refresh(self):
        """
        called by the main display loop
        """
        self.time += 1
        self.image = pygame.Surface((self.size,self.size)).convert_alpha()
        self.image.fill(self.cl+[110-(self.time*10)])
        if self.time > 10:
            return 0
        return 1

class PGText(pygame.sprite.Sprite):
    def __init__(self,pos,cl,text):
        """
        sprite corresponding to text written on the screen
        """
        pygame.sprite.Sprite.__init__(self, self.containers)
        filename = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..","data","font","Play-Regular.ttf")
        font = pygame.font.Font(filename, 14)
        if text.startswith("BBB"):
            font = pygame.font.Font(filename, 36)
            text = text[3:]
        self.cl = cl
        self.text = text
        self.image = font.render( text, 1, cl)
        self.rect = self.image.get_rect()
        self.rect.left = int(round(pos[0]))
        self.rect.top = int(round(pos[1]))
        self.time = 0

class PGBonus(pygame.sprite.Sprite):
    def __init__(self,pos,r,images):
        """
        sprite corresponding to a bonus
        """
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.images = images
        self.r = r
        self.image = images[0]
        self.rect = self.image.get_rect()
        self.rect.left = pos[0]
        self.rect.top = pos[1]
        self.time = 0

    def refresh(self):
        """
        called by the main display loop
        """
        self.time += 1
        if self.time%3 == 0:
            self.image = self.images[(self.time/3)%len(self.images)]
        if self.time > 200:
            return 0
        return 1

class Display:
    def __init__(self):
        """
        Everything related to display, using pygame
        """
        self.core = None
        
        self.mode = "game"   #can be "game", "menu", "pause"
        self.menu = Menu(self)
        
        self.screen_width = 800
        self.screen_height = 600
        self.screen_winstyle = 0
        self.screen = None
        self.bkgd = None
        self.bkgd_first = 0 #0 if the bkgd should be redrawn
        self.clock = None
        self.music = None
        
        self.unit_size = 20 #size in pixel of the units

        self.pgunits = [] #pgunit objects
        self.pgbullets = [] #pgunit objects
        self.spgunit = None #sprite for pgunit
        self.pgshadows = [] #pgshadow objects
        self.spgshadow = None #sprite for pgshadow
        self.pgtexts= []
        self.spgtext = None #sprite for pgtext
        self.pgbonuss = [] #pgbonus objects
        self.spgbonus = None #sprite for pgbonus

        self.key_pressed = [] #list of keys currently pressed

        self.colors = [] #colors of the sides

        self.bonus_images = {} #Surface images for bonuses
        self.medal_images = {} #Surface images for medals
        self.sounds = {} #Sound objects for the sound effects

    def init(self):
        """
        Initiallize everything needed for pygame
        Called in Core.load()
        """
        if not pygame.image.get_extended():
            raise SystemExit, "Sorry, extended image module required"

        pygame.init()
        if pygame.mixer and not pygame.mixer.get_init():
            print 'Warning, no sound'
            pygame.mixer = None

        #start music in a new thread, because it introduces
        #a delay otherwise
        thread.start_new(self.load_music,())
        
        pygame.display.gl_set_attribute(pygame.GL_DEPTH_SIZE, 16)
        pygame.display.gl_set_attribute(pygame.GL_STENCIL_SIZE, 1)
        pygame.display.gl_set_attribute(pygame.GL_ALPHA_SIZE, 8)
        self.set_fullscreen(self.core.config.fullscreen,0)

        self.clock = pygame.time.Clock()
        
        self.set_colors()

        self.set_background()
        self.screen.blit(self.bkgd, (0,0))

        filename = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..","data","px","icon.png")
        try:
            icon = pygame.image.load(filename)
        except pygame.error:
            raise SystemExit, 'Could not load image "%s" %s'%(filename, pygame.get_error())
        pygame.display.set_icon(icon)
        pygame.display.set_caption('ObPortum')
        pygame.mouse.set_visible(1)

        self.load_bonus()
        self.load_medals()
        self.load_sound()

        #no need for LayeredUpdate
        self.spgshadow = pygame.sprite.RenderUpdates()
        self.spgbonus = pygame.sprite.RenderUpdates()
        self.spgunit = pygame.sprite.RenderUpdates()
        self.spgtext = pygame.sprite.RenderUpdates()
        PGShadow.containers = self.spgshadow
        PGBonus.containers = self.spgbonus
        PGUnit.containers = self.spgunit
        PGText.containers = self.spgtext

        #initialize the menu tool
        self.menu.init()

    def set_colors(self):
        """
        Choose the colors for the different teams
        """
        self.colors = []
        self.colors.append([47,151,255])
        self.colors.append([255,12,48])
        self.colors.append([255,127,8])
        self.colors.append([197,71,255])
        self.colors.append([190,255,25])
        self.colors.append([64,255,121])
        if self.core.config.color == "whiterandom":
            random.shuffle(self.colors)
            self.colors = [[255,255,255]]+self.colors
        elif self.core.config.color == "random":
            self.colors.append([255,255,255])
            random.shuffle(self.colors)
        elif self.core.config.color == "whitebluered":
            self.colors = [[255,255,255]]+self.colors
        else:
            #unsafe, but if someone can modify save.cfg, he can
            #modify any py file too
            self.colors = eval(self.core.config.color)

    def set_fullscreen(self,i,redraw=True):
        """
        Toggle fullscreen.
        argument:
          i: 1=fullscreen, 0=window
          redraw: call for redrawing
        """
        r = (self.screen_width, self.screen_height)
        bestdepth = pygame.display.mode_ok(r, self.screen_winstyle, 32)
        if i:
            self.screen = pygame.display.set_mode(r, pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.FULLSCREEN, bestdepth)
        else:
            self.screen = pygame.display.set_mode(r, pygame.HWSURFACE | pygame.DOUBLEBUF, bestdepth)
        if redraw:
            self.redraw()

    def load_music(self):
        filename = os.path.join(os.path.abspath(os.path.dirname(__file__)), 
                                "..","data","music",
                                "Lobo_Loco_-_01_-_Oortsche_Wolke_ID_80.ogg")
        try:
            self.music = pygame.mixer.Sound(filename)
            self.music.play(-1)
        except:
            print "Failed to play %s"%filename
        self.change_volume_music()

    def load_sound(self):
        if not pygame.mixer:
            return
        filenames = ["achievement","button_cl","button_ov","death","hit","laser"]
        for fn in filenames:
            filename = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..","data","sound","%s.wav"%fn)
            try:
                self.sounds[fn] = pygame.mixer.Sound(filename)
            except:
                print "Failed to play %s"%filename
        self.change_volume_sound()

    def play_sound(self,na,loop=0):
        """
        Play the sound effect
        arguments:
          na: the sound name, as loaded in load_sound
        """
        if na not in self.sounds.keys():
            return
        if self.core.config.sound1 == 0:
            return
        try:
            if loop:
                self.sounds[na].play(-1)
            else:
                self.sounds[na].play()
        except:
            print "Failed to play %s"%na

    def change_volume_sound(self):
        """
        Apply the config sound effect volume changes
        """
        self.sound_volume = {"achievement":1.,
                             "button_cl":0.5,
                             "button_ov":0.5,
                             "laser":0.1,
                             "hit":0.1,
                             "death":1.
                         }
        for fn in self.sounds:
            factor1 = self.sound_volume.get(fn,1.)
            factor2 = self.core.config.get_sound1()
            self.sounds[fn].set_volume(factor1*factor2)

    def change_volume_music(self):
        """
        Apply the config music volume changes
        """
        if self.music:
            factor1 = self.core.config.get_sound2()
            self.music.set_volume(factor1)

    def load_bonus(self):
        """
        Load image for the bonuses
        """
        filenames = ["r1","r2","r3","r4","li","bu"]
        for fn in filenames:
            filename = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..","data","px","bonus_%s.png"%fn)
            try:
                self.bonus_images["%s0"%fn] = pygame.image.load(filename).convert_alpha()
                for i in range(1,7):
                    self.bonus_images["%s%i"%(fn,i)] = self.bonus_images["%s0"%fn].copy()
                    self.bonus_images["%s%i"%(fn,i)].fill((255, 255, 255, 255-(30*i)), None, pygame.BLEND_RGBA_MULT)
            except pygame.error:
                raise SystemExit, 'Could not load image "%s" %s'%(filename, pygame.get_error())

    def load_medals(self):
        """
        Load image for the medals
        """
        filenames = ["00","01","02","03","04","05","06","07","08","09"]
        for fn in filenames:
            filename = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..","data","px","medal_%s.png"%fn)
            self.medal_images["%s"%fn] = pygame.image.load(filename).convert_alpha()

    def medal_paint(self,name,desc):
        """
        Draw the medals based on description
        arguments:
          name: keyname for the medal_images dico
          desc: array of [imagename, (r,g,b)]
        """
        w = self.medal_images["00"].get_rect().width
        h = self.medal_images["00"].get_rect().height
        image = pygame.Surface((w, h)).convert_alpha()

        for l in desc:
            if len(l) == 1:
                image.blit(self.medal_images[l[0]],(0,0))
            if len(l) == 2:
                piece = self.medal_images[l[0]].copy().convert_alpha()
                arr = pygame.surfarray.pixels3d(piece)
                #arr[:,:,0] += l[1][0]-255
                arr[:,:,0] = arr[:,:,0]+l[1][0]-255
                arr[:,:,1] = arr[:,:,1]+l[1][1]-255
                #arr[:,:,1] += l[1][1]-255
                arr[:,:,2] = arr[:,:,2]+l[1][2]-255
                del arr
                image.blit(piece,(0,0))

        self.medal_images[name] = image

    def game_over(self):
        self.pgtexts.append(PGText([(self.screen_width/2)-100,(self.screen_height/2)-100],self.colors[0],"BBBGame Over"))
        self.pgtexts[-1].time = 300

    def message(self,text):
        """
        Print a message in the bottom left
        argument:
          text: text to display
        """
        for t in self.pgtexts:
            if t.rect.top > (self.screen_height/2):
                t.rect.top += -20
        self.pgtexts.append(PGText([8,self.screen_height-20],self.colors[0],text))
        self.pgtexts[-1].time = 100

    def create_bonus(self,pos,r):
        """
        Create a bonus sprite of the flavor 'r' in the screen position 'pos'
        """
        images = []
        for i in range(7):
            images.append(self.bonus_images["%s%i"%(r,i)])
        for i in range(7):
            images.append(self.bonus_images["%s%i"%(r,6-i)])
        self.pgbonuss.append(PGBonus(pos,r,images))
    
    def associate_bullet(self,unit):
        """
        if not done yet, it will associate an Unit (from Core) to a PGUnit (from Display)
        """
        if unit.pgunit:
            return
        unit.pgunit = PGUnit(unit,self,1)
        self.pgbullets.append(unit.pgunit)

    def associate_unit(self,unit):
        """
        if not done yet, it will associate an Unit (from Core) to a PGUnit (from Display)
        """
        if unit.pgunit:
            return
        unit.pgunit = PGUnit(unit,self,0)
        self.pgunits.append(unit.pgunit)

    def set_background(self):
        """
        create the background image from data/bkgd.png
        """
        if self.bkgd:
            return
        bg = None
        self.bkgd = pygame.Surface((self.screen_width, self.screen_height))
        filename = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..","data","px","bkgd.png")
        try:
            bg = pygame.image.load(filename)
        except pygame.error:
            raise SystemExit, 'Could not load image "%s" %s'%(filename, pygame.get_error())
        for x in range(int(self.screen_width/bg.get_width())):
            for y in range(int(self.screen_height/bg.get_height())):
                self.bkgd.blit(bg, (x*bg.get_width(),y*bg.get_height()))

    def check_key(self):
        """
        keyboard events
        """
        border = 100
        if not self.core.units[0]:
            return
        for key in self.key_pressed:
            if key == K_UP or key == K_z or key == K_w:
                if self.core.units[0][0].pos[1] > -1*border:
                    self.core.units[0][0].move([0,-1])
            elif key == K_DOWN or key == K_s:
                if self.core.units[0][0].pos[1] < self.screen_height+border:
                    self.core.units[0][0].move([0,1])
            elif key == K_LEFT or key == K_a or key == K_q:
                if self.core.units[0][0].pos[0] > -1*border:
                    self.core.units[0][0].move([-1,0])
            elif key == K_RIGHT or key == K_d:
                if self.core.units[0][0].pos[0] < self.screen_width+border:
                    self.core.units[0][0].move([1,0])

    def mousepress(self,button,xy):
        """
        mouse events
        """
        if not self.core.units[0]:
            return
        if button[0] == 1:
            self.core.shoot(0,0,xy)

    def create_shadow(self,pgunit):
        """
        Create a sprite for the shadow, associated to the sprite 'pgunit'
        """
        self.pgshadows.append(PGShadow(pgunit))

    def kill(self,unit):
        """
        Remove the PGUnit sprite in a clean way
        argument:
          unit: PGUnit to remove
        """
        self.spgunit.clear(self.screen, self.bkgd)
        if unit.what == 1:
            if unit in self.pgbullets:
                self.pgbullets.remove(unit)
        if unit.what == 0:
            if unit in self.pgunits:
                self.pgunits.remove(unit)
            else:
                print "uh?",unit.Rect,[u.Rect for u in self.pgunits]
        unit.kill()

    def draw_text(self):
        """
        Draw the usual top left messages
        """
        k = self.core.units.keys()
        k.sort()
        y = 8
        for kk in k:
            if kk == 0:
                continue
            number = len(self.core.units[kk])
            t = "s"
            if number < 2:
                t = ""
            self.pgtexts.append(PGText([10,y],self.colors[kk],"%i unit%s, %.1f, %s"%(number,t,self.core.ratio[kk],self.core.status(kk))))
            y += 16
        if (0 in k) and len(self.core.units[0]):
            self.pgtexts.append( PGText([10,y],self.colors[0],"%i health, %i ammo"%(self.core.units[0][0].health,self.core.units[0][0].ammo)))
        else:
            self.pgtexts.append( PGText([10,y],self.colors[0],"you are dead"))
        self.pgtexts.append(PGText([self.screen_width-75,8],self.colors[0],"%s"%(self.core.get_player_time())))


    def check_events(self):
        """
        Check events during self.mode == 'game'
        """
        for event in pygame.event.get():
            if event.type == ACTIVEEVENT:
                #turn on pause if the window is losing focus
                if self.core.config.pausewhenout:
                    if event.state == 2 and event.gain == 0:
                        self.mode = "pause"
            elif event.type == QUIT:
                #quit
                self.core.play = 0
            elif (event.type == KEYDOWN and event.key == K_ESCAPE) or\
                 (event.type == KEYDOWN and event.key == K_p):
                #pause
                self.mode = "pause"
            elif event.type == KEYDOWN:
                #keydown: add it to key_pressed
                if event.key not in self.key_pressed:
                    self.key_pressed.append(event.key)
                if event.key == K_r:
                    self.core.new_game()
            elif event.type == KEYUP:
                #keyup: remove it to key_pressed
                if event.key in self.key_pressed:
                    self.key_pressed.remove(event.key)
            elif event.type == MOUSEBUTTONDOWN:
                #mouse click
                self.mousepress(pygame.mouse.get_pressed(),pygame.mouse.get_pos())
        #look which keys are pressed in key_pressed
        self.check_key()

    def update(self):
        """
        Update everything to be updated on the screen
        Used in the main loop
        Called in Core.run()
        """
        #mouse and key event
        if self.mode == "game":
            self.check_events()
        elif self.mode == "menu" or self.mode == "pause":
            self.menu.check_events()

        if self.mode != "pause":
            #units
            for u in self.pgunits:
                u.refresh()
                if u.unit.side == 0:
                    collision = pygame.sprite.spritecollide(u,self.pgbonuss, False)
                    if collision:
                        for l in collision:
                            l.time = 200
                            self.core.get_bonus(l.r)
            #bullets
            for u in self.pgbullets[:]:
                r = u.unit.step()
                if r != 0:
                    u.refresh()
                    collision = pygame.sprite.spritecollide(u,self.pgunits, False)
                    if collision:
                        for l in collision:
                            if l.unit.side != u.unit.side:
                                self.core.hit(l.unit,u.unit)
                else:
                    self.core.kill(u.unit)
            #shadows
            for u in self.pgshadows[:]:
                r = u.refresh()
                if r==0:
                    self.pgshadows.remove(u)
                    u.kill()
            #bonus
            for u in self.pgbonuss[:]:
                r = u.refresh()
                if r==0:
                    self.core.kill_bonus(u)
                    self.pgbonuss.remove(u)
                    u.kill()
            #texts
            for t in self.pgtexts[:]:
                if t.time <= 0:
                    t.kill()
                    self.pgtexts.remove(t)
                else:
                    t.time += -1
            if self.mode == "game":
                self.draw_text()
    
            #update
            self.spgshadow.clear(self.screen, self.bkgd)
            self.spgbonus.clear(self.screen, self.bkgd)
            self.spgunit.clear(self.screen, self.bkgd)
            self.spgtext.clear(self.screen, self.bkgd)
        if self.mode == "menu" or self.mode == "pause":
            self.menu.update()
        dirty = []
        if self.mode != "pause":
            dirty.append(self.spgshadow.draw(self.screen))
            dirty.append(self.spgbonus.draw(self.screen))
            dirty.append(self.spgunit.draw(self.screen))
            dirty.append(self.spgtext.draw(self.screen))

        if self.mode == "menu" or self.mode == "pause":
            dirty += self.menu.dirty()

        for l in dirty:
            pygame.display.update(l)

        if self.bkgd_first == 0:
            self.bkgd_first = 1
            pygame.display.update()
        self.clock.tick(self.core.fps)

    def reset(self):
        """
        Destroy the sprite and redraw
        """
        #destroy the bonus and texts
        for u in self.pgshadows:
            u.kill()
        for u in self.pgbonuss:
            u.kill()
        for t in self.pgtexts:
            t.kill()
        self.pgshadows = []
        self.pgbonuss = []
        self.pgtexts = []
        #redraw the background
        self.redraw()

    def redraw(self):
        """
        Redraw
        """
        self.screen.blit(self.bkgd, (0,0))
        self.bkgd_first = 0

    def end(self):
        """
        Before closing the application
        Called in Core.Run()
        """
        pass
