#! /usr/bin/env python

#    This file is part of ObPortum.
#
#    ObPortum is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ObPortum is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ObPortum.  If not, see <http://www.gnu.org/licenses/>.

#import basic pygame modules
try:
    import pygame
    from pygame.locals import *
except ImportError:
    print("pygame 1.9.1 or higher is required")

import os
import time

class PGMenu(pygame.sprite.Sprite):
    def __init__(self,menu,options):
        """
        Sprite corresponding to an elemet of the menu
        """
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.menu = menu
        self.options = options
        if options.get("type") == "bkgd":
            #background of the panel
            xb = options.get("xborder",0)
            height = options.get("height",menu.display.screen_height-120)
            self.image = pygame.Surface((menu.display.screen_width-120-xb-xb,height)).convert_alpha()
            self.rect = self.image.get_rect()
            self.rect.left = 60+xb
            self.rect.top = (menu.display.screen_height-height)/2
            self.image.fill((0, 0, 0, 200))
        if options.get("type") == "cursor":
            #cursor (white square)
            self.image = pygame.Surface((10,10)).convert_alpha()
            self.rect = self.image.get_rect()
            self.rect.left = -100
            self.rect.top = -100
            self.image.fill((255, 255, 255))
            self.value = -1
        if options.get("type") == "text":
            #text
            filename = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..","data","font","Play-Regular.ttf")
            size = options.get("size",16)
            font = pygame.font.Font(filename, size)
            self.cl = options.get("cl",(255,255,255))
            self.text = options.get("text","")
            self.image = font.render( self.text, 1, self.cl)
            self.rect = self.image.get_rect()
            pos = options.get("pos",[0,0])
            self.rect.left = int(round(pos[0]))
            self.rect.top = int(round(pos[1]))
        if options.get("type") == "button":
            #button
            #added to self.menu.buttons
            filename = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..","data","font","Play-Regular.ttf")
            font = pygame.font.Font(filename, 18)
            self.cl = options.get("cl",(255,255,255))
            self.text = options.get("text","")
            text = font.render( self.text, 1, self.cl)
            w = options.get("size",100)
            self.image = pygame.Surface((w+1,25+1)).convert_alpha()
            pygame.draw.line(self.image,self.cl,(0,0),(w,0))
            pygame.draw.line(self.image,self.cl,(0,25),(w,25))
            pygame.draw.line(self.image,self.cl,(0,0),(0,25))
            pygame.draw.line(self.image,self.cl,(w,0),(w,25))
            self.image.blit(text, (25,1))
            self.rect = self.image.get_rect()
            pos = options.get("pos",[0,0])
            self.rect.left = int(round(pos[0]))
            self.rect.top = int(round(pos[1]))
            self.menu.buttons.append(self)
            self.value = options.get("value",self.text)
        if options.get("type") == "bonusimage":
            #image from self.display.bonus_images
            self.image = menu.display.bonus_images[options.get("value","li0")]
            self.rect = self.image.get_rect()
            pos = options.get("pos",[0,0])
            self.rect.left = int(round(pos[0]))
            self.rect.top = int(round(pos[1]))
        if options.get("type") == "medalimage":
            #image from self.display.medal_images
            v = options.get("value","00")
            self.image = menu.display.medal_images.get(v,menu.display.medal_images["00"])
            self.rect = self.image.get_rect()
            pos = options.get("pos",[0,0])
            self.rect.left = int(round(pos[0]))
            self.rect.top = int(round(pos[1]))
        if options.get("type") == "plot":
            #draw a plot
            w = options.get("w",100)
            h = options.get("h",100)
            cl = options.get("cl",(255,255,255))
            self.image = pygame.Surface((w+2,h+2)).convert_alpha()
            self.image.fill((0, 0, 0, 0))
            val = options.get("values",[])
            ext = options.get("extr",[0,1])
            last_point = None
            dy = (ext[1]-ext[0])
            if dy == 0:
                dy = 1
            for v in val:
                new_point = [int(round(v[0]*w)),h-int(round((v[1]-ext[0])*h*1./dy))]
                if last_point:
                    pygame.draw.line(self.image,cl,last_point,new_point)
                last_point = new_point[:]
            self.rect = self.image.get_rect()
            pos = options.get("pos",[0,0])
            self.rect.left = int(round(pos[0]))
            self.rect.top = int(round(pos[1]))

class Menu:
    def __init__(self,display):
        """
        Tool to display the menu
        """
        self.display = display
        self.spgmenu = None
        self.elements = [] #all sprite object created here
        self.buttons = []  #buttons
        self.cursor = None #Sprite for the cursor
        self.where = "main"#Where in the menu we are
        self.refreshed = 0 #Recreate, to set to 1 when the menu changes

    def init(self):
        """
        Called by Display.init()
        Init layers and containers
        """
        self.spgmenu = pygame.sprite.LayeredUpdates()
        PGMenu.containers = self.spgmenu

    def set_cursor(self,i,silent=0):
        """
        Move the cursor to the position of the button
        of index 'i'.
        Do a click sound if silent!=1
        """
        if self.cursor.value == i:
            #already in place
            return
        self.cursor.value = i
        if i >= len(self.buttons):
            #no button for this index
            self.cursor.rect.left = -100
            return
        if silent == 0:
            self.display.play_sound("button_ov")
        #place the sprite
        self.cursor.rect.left = self.buttons[i].rect.left+6
        self.cursor.rect.top = self.buttons[i].rect.top+(self.buttons[i].rect.height/2)-(self.cursor.rect.height/2)

    def move_cursor(self,i):
        """
        Move the cursor up or down
        argument:
          i: 1 going down, -1 going up
        """
        new_value = self.cursor.value + i
        if new_value < 0:
            new_value = len(self.buttons)-1
        if new_value >= len(self.buttons):
            new_value = 0
        self.set_cursor(new_value)

    def display_main(self):
        """
        Create Sprite for the main menu
        """
        xb = 220
        self.elements.append(PGMenu(self,{"type":"bkgd","xborder":xb,"height":310}))
        x = self.elements[-1].rect.left+10
        y = self.elements[-1].rect.top+10
        options = {"type":"text","text":"Ob Portum","size":24,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))
        bw = self.display.screen_width-120-xb-xb-20
        y += 50
        options = {"type":"button","text":"play","size":bw,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))
        y += 40
        options = {"type":"button","text":"high scores","size":bw,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))
        y += 40
        options = {"type":"button","text":"achievements","size":bw,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))
        y += 40
        options = {"type":"button","text":"options","size":bw,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))
        y += 40
        options = {"type":"button","text":"help","size":bw,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))
        y += 40
        options = {"type":"button","text":"quit","size":bw,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))

        self.cursor = PGMenu(self,{"type":"cursor"})
        self.set_cursor(0,silent=1)

    def display_help(self):
        """
        Create Sprite for the help menu
        """
        xb = 110
        self.elements.append(PGMenu(self,{"type":"bkgd","xborder":xb}))
        x = 70+xb
        options = {"type":"text","text":"Help","size":24,"pos":[x,70]}
        self.elements.append(PGMenu(self,options))
        bw = self.display.screen_width-120-xb-xb-20
        text = []
        if self.where == "help":
            #first panel
            options = {"type":"button","text":"controls","value":"help_co","size":bw,"pos":[x,120]}
            self.elements.append(PGMenu(self,options))
            options = {"type":"button","text":"gameplay","value":"help_gp","size":bw,"pos":[x,160]}
            self.elements.append(PGMenu(self,options))
            options = {"type":"button","text":"bonus","value":"help_bo","size":bw,"pos":[x,200]}
            self.elements.append(PGMenu(self,options))
            options = {"type":"button","text":"about","value":"help_ab","size":bw,"pos":[x,240]}
            self.elements.append(PGMenu(self,options))
            options = {"type":"button","text":"back","value":"main","size":bw,"pos":[x,280]}
            self.elements.append(PGMenu(self,options))
            self.cursor = PGMenu(self,{"type":"cursor"})
            self.set_cursor(4,silent=1)
        elif self.where == "help_co":
            #controls panel
            text = ["During a game:",
                    "  use arrow keys or ASDW or QSDZ keys to move",
                    "  use left-click to fire",
                    "  use P or ESC to get the pause menu",
                    "  use R to restart the game","",
                    "In the menu:",
                    "  use arrow keys or the mouse to move in the menu items",
                    "  use RETURN, SPACE or left-click to select",
                    "  use ESC to go back in the main menu or quit",
                ]
        elif self.where == "help_gp":
            #gameplay panel
            text = ["During the game, two teams are fighting.","",
                    "The diplomatic level to a given team will change if:",
                    "- you take an arrow bonus box",
                    "- you attack them or their enemies",
                    "    (especially when the numbers are not balanced)",
                    "- you don't attack their enemies for too long","",
                    "The aggressivity and damage of one team also depend",
                    "  of the diplomatic level.","",
                    "Your goal is to survive as long as possible (you can",
                    "  try to balance the diplomatic levels) or to get the",
                    "  different achievements (for each difficulty setting).",
                ]
        elif self.where == "help_bo":
            #bonus panel
            text = ["The white blinking boxes are bonuses (or maluses) ",
                    "  that you can collect.","",
                    "        more health",
                    "        more ammo",
                    "        increase the friendliness of the most friendly team",
                    "        decrease the friendliness of the most friendly team",
                    "        increase the friendliness of the less friendly team",
                    "        decrease the friendliness of the less friendly team",
                ]
            options = {"type":"bonusimage","value":"li0","pos":[x+5,200]}
            self.elements.append(PGMenu(self,options))
            options = {"type":"bonusimage","value":"bu0","pos":[x+5,230]}
            self.elements.append(PGMenu(self,options))
            options = {"type":"bonusimage","value":"r20","pos":[x+5,260]}
            self.elements.append(PGMenu(self,options))
            options = {"type":"bonusimage","value":"r10","pos":[x+5,290]}
            self.elements.append(PGMenu(self,options))
            options = {"type":"bonusimage","value":"r30","pos":[x+5,320]}
            self.elements.append(PGMenu(self,options))
            options = {"type":"bonusimage","value":"r40","pos":[x+5,350]}
            self.elements.append(PGMenu(self,options))

        elif self.where == "help_ab":
            #about panel
            text = ['"Ob portum" means "to the harbor" in latin.',
                    'It is the etymology of the word "opportune",',
                    '  itself the origin of "opportunistic".',
                    "",
                    "Created for pyweek 23",
                    "https://pyweek.org",
                    "February 25th 2017","",
                    "by Cauch","",
                    "GNU GPL3 licence, sources available here:",
                    "https://gitlab.com/cauch/ObPortum",
                ]
        #for the subpanel, it's always just text to display + 'back' button
        options = {"type":"text","text":"","pos":[x,120-30]}
        if text:
            for t in text:
                if t: 
                    options["text"] = t
                    options["pos"][1] += 30
                    self.elements.append(PGMenu(self,options))
                else:
                    options["pos"][1] += 15                    
            options["pos"][1] += 40
            options = {"type":"button","text":"back","value":"help","size":bw,"pos":[x,options["pos"][1]]}
            self.elements.append(PGMenu(self,options))
            self.cursor = PGMenu(self,{"type":"cursor"})
            self.set_cursor(0,silent=1)

    def display_firsttime(self):
        """
        Create Sprite for the panel shown the first time you play
        """
        xb = 110
        self.elements.append(PGMenu(self,{"type":"bkgd","xborder":xb}))
        x = 70+xb
        options = {"type":"text","text":"Welcome to","size":24,"pos":[x,70]}
        self.elements.append(PGMenu(self,options))
        options = {"type":"text","text":"Ob Portum","size":36,"pos":[x+30,110]}
        self.elements.append(PGMenu(self,options))
        bw = self.display.screen_width-120-xb-xb-20
        text = []
        text = ["Two teams are fighting.",
                "Fight and balance your diplomacy level to stay alive as long","  as possible.",
                "Or try to get all achievements for all difficulty levels.","",
                "Use arrow keys (or ASDW or QSDZ) to move.",
                "Use left-click to fire.","",
                "More info in the help section."
        ]
        options = {"type":"text","text":"","pos":[x,150]}
        if text:
            for t in text:
                if t: 
                    options["text"] = t
                    options["pos"][1] += 30
                    self.elements.append(PGMenu(self,options))
                else:
                    options["pos"][1] += 15                    
            options["pos"][1] += 40
            options = {"type":"button","text":"start","value":"main","size":bw,"pos":[x,options["pos"][1]]}
            self.elements.append(PGMenu(self,options))
            self.cursor = PGMenu(self,{"type":"cursor"})
            self.set_cursor(0,silent=1)


    def display_pause(self):
        """
        Create Sprite for the pause menu
        """
        xb = 220
        self.elements.append(PGMenu(self,{"type":"bkgd","xborder":xb,"height":350}))
        x = self.elements[-1].rect.left+10
        y = self.elements[-1].rect.top+10
        options = {"type":"text","text":"Pause","size":24,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))
        bw = self.display.screen_width-120-xb-xb-20
        y += 50
        options = {"type":"button","text":"continue","value":"pause_co","size":bw,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))
        y += 40
        options = {"type":"button","text":"restart","value":"play","size":bw,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))
        y += 40
        options = {"type":"button","text":"high scores","size":bw,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))
        y += 40
        options = {"type":"button","text":"achievements","value":"pauseac","size":bw,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))
        if self.display.core.config.plots:
            y += 40
            options = {"type":"button","text":"plots","size":bw,"pos":[x,y]}
            self.elements.append(PGMenu(self,options))
        y += 40
        options = {"type":"button","text":"quit to menu","value":"pause_ma","size":bw,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))
        y += 40
        options = {"type":"button","text":"quit","value":"pause_qu","size":bw,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))
        self.cursor = PGMenu(self,{"type":"cursor"})
        self.set_cursor(0,silent=1)

    def display_achiev(self):
        """
        Create Sprite for the achievement menu
        """
        xb = 110
        self.elements.append(PGMenu(self,{"type":"bkgd","xborder":xb}))
        x = 70+xb
        options = {"type":"text","text":"Achievements","size":24,"pos":[x,70]}
        self.elements.append(PGMenu(self,options))
        options = {"type":"text","text":'for difficulty "%s"'%self.display.core.config.difficulty,"size":16,"pos":[x+165,70+7]}
        self.elements.append(PGMenu(self,options))
        bw = self.display.screen_width-120-xb-xb-20
        y = 50

        part = 0 #only 6 achiev displayed, circle around them
        if "_" in self.where:
            part = int(self.where.split("_")[-1])
        ka = self.display.core.achiev.keys()
        ka1 = [[self.display.core.achiev[a].title,a] for a in ka if a not in self.display.core.achiev_open]
        ka2 = [[self.display.core.achiev[a].title,a] for a in ka if a in self.display.core.achiev_open]
        ka1.sort()
        ka2.sort()
        ka = ka1+ka2
        if part*6 >= len(ka):
            part = 0

        #6 achiev
        for a in ka[(part*6):(part*6)+6]:
            y += 55
            achiev = self.display.core.achiev[a[1]]
            tt = a[1]
            #if a[1] in self.display.core.achiev_open:
            #    tt = "00"
            options = {"type":"medalimage","value":tt,"pos":[x,y]}
            self.elements.append(PGMenu(self,options))
            options = {"type":"text","pos":[x+30,y-8],"text":achiev.title,"size":20}
            self.elements.append(PGMenu(self,options))
            options = {"type":"text","pos":[x+30,y+13],"text":achiev.description[0]}
            self.elements.append(PGMenu(self,options))
            options = {"type":"text","pos":[x+30,y+28],"text":achiev.description[1]}
            self.elements.append(PGMenu(self,options))

        #'next' and 'back' buttons
        if self.display.mode != "pause":
            options = {"type":"button","text":"next","value":"achiev_%i"%(part+1),"size":bw,"pos":[x,380+60]}
        else:
            options = {"type":"button","text":"next","value":"pauseac_%i"%(part+1),"size":bw,"pos":[x,380+60]}
        self.elements.append(PGMenu(self,options))
        options = {"type":"button","text":"back","value":"main","size":bw,"pos":[x,380+100]}
        self.elements.append(PGMenu(self,options))
        self.cursor = PGMenu(self,{"type":"cursor"})
        self.set_cursor(0,silent=1)

    def display_highscores(self):
        """
        Create Sprite for the high score menu
        """
        xb = 110
        self.elements.append(PGMenu(self,{"type":"bkgd","xborder":xb,"height":310}))
        x = self.elements[-1].rect.left+10
        y = self.elements[-1].rect.top+10
        options = {"type":"text","text":"High scores","size":24,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))
        options = {"type":"text","text":'for difficulty "%s"'%self.display.core.config.difficulty,"size":16,"pos":[x+140,y+7]}
        self.elements.append(PGMenu(self,options))
        bw = self.display.screen_width-120-xb-xb-20
        hs = self.display.core.config.get_highscores()
        y += 20
        for h in hs:
            y += 35
            t1 = h.split("_")[0]
            t2 = ", ".join(h.split("_")[1:])
            options = {"type":"text","pos":[x+5+100,y+4],"text":t2,"size":16}
            self.elements.append(PGMenu(self,options))
            options = {"type":"text","pos":[x+5,y],"text":t1,"size":20}
            self.elements.append(PGMenu(self,options))
        options = {"type":"button","text":"back","value":"main","size":bw,"pos":[x,y+60]}
        self.elements.append(PGMenu(self,options))
        self.cursor = PGMenu(self,{"type":"cursor"})
        self.set_cursor(0,silent=1)

    def display_options(self):
        """
        Create Sprite for the option menu
        """
        xb = 110
        self.elements.append(PGMenu(self,{"type":"bkgd","xborder":xb}))
        x = self.elements[-1].rect.left+10
        y = self.elements[-1].rect.top+10
        options = {"type":"text","text":"Options","size":24,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))
        bw = self.display.screen_width-120-xb-xb-20

        #if a button has been clicked, apply the change to self.display.core.config
        cfg = self.display.core.config
        w1 = self.where.split("_")[-1]
        if "_" in self.where:
            cfg.rotate(w1)
            cfg.save()
                
        y += 40
        options = {"type":"button","text":"difficulty: %s"%cfg.print_dif(),"value":"options_dif","size":bw,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))
        y += 40
        options = {"type":"button","text":"color: %s"%cfg.print_col(),"value":"options_col","size":bw,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))
        y += 40
        options = {"type":"button","text":"sound volume: %s"%cfg.print_sound(1),"value":"options_so1","size":bw,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))
        y += 40
        options = {"type":"button","text":"music volume: %s"%cfg.print_sound(2),"value":"options_so2","size":bw,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))
        y += 40
        options = {"type":"button","text":"fullscreen: %s"%cfg.print_fs(),"value":"options_fsc","size":bw,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))
        y += 40
        options = {"type":"button","text":"plot functionality %s"%cfg.print_plo(),"value":"options_plo","size":bw,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))
        y += 40
        options = {"type":"button","text":"pause-when-unfocus %s"%cfg.print_pwo(),"value":"options_pwo","size":bw,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))

        y += 40
        options = {"type":"button","text":"back","value":"main","size":bw,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))
        self.cursor = PGMenu(self,{"type":"cursor"})
        w2 = ["dif","col","so1","so2","fsc","plo","pwo","options"]
        curpos = w2.index(w1)
        self.set_cursor(curpos,silent=1)

    def display_plots(self):
        """
        Create Sprite for the plot menu
        """
        xb = 110
        self.elements.append(PGMenu(self,{"type":"bkgd","xborder":xb,"height":310}))
        x = self.elements[-1].rect.left+10
        y = self.elements[-1].rect.top+10
        options = {"type":"text","text":"Plots","size":24,"pos":[x,y]}
        self.elements.append(PGMenu(self,options))
        bw = self.display.screen_width-120-xb-xb-20
        number = 7
        w1 = number
        if "_" in self.where:
            w1 = int(self.where.split("_")[1])

        valuess = []
        mm = []
        cl = []
        text = ""
        if w1 == 0 or w1 == number:
            #health and ammo
            va1,mm1 = self.display.core.h.plot("health")
            valuess.append(va1)
            va2,mm2 = self.display.core.h.plot("ammo")
            valuess.append(va2)
            mm = [min(mm1[0],mm2[0]),max(mm1[1],mm2[1])]
            cl = [[100,100,255],[255,100,100]]
            text = "health and ammo"
            mm[0] = 0
            valuess.append([[0,0],[1,0]])
            cl.append([20,20,20])
        elif w1 == 1:
            #ennemy number
            for i in range(self.display.core.nside):
                va,m = self.display.core.h.plot("en%in"%(i))
                valuess.append(va)
                if not mm:
                    mm = m[:]
                else:
                    mm = [min(mm[0],m[0]),max(mm[1],m[1])]
                cl.append(self.display.colors[i+1])
            text = "number of units"
        elif w1 == 2:
            #ennemy relationship
            for i in range(self.display.core.nside):
                va,m = self.display.core.h.plot("en%ir"%(i))
                valuess.append(va)
                if not mm:
                    mm = m[:]
                else:
                    mm = [min(mm[0],m[0]),max(mm[1],m[1])]
                cl.append(self.display.colors[i+1])
            valuess.append([[0,0],[1,0]])
            cl.append([20,20,20])
            text = "relationship"
        elif w1 == 3:
            #player related hit, done
            for i in range(self.display.core.nside):
                va,m = self.display.core.h.plot("hdp%i"%(i+1))
                if not mm:
                    mm = m[:]
                else:
                    mm = [min(mm[0],m[0]),max(mm[1],m[1])]
                valuess.append(va)
                cl.append(self.display.colors[i+1])
            text = "hits done by the player"
        elif w1 == 4:
            #player related hit, received
            for i in range(self.display.core.nside):
                va,m = self.display.core.h.plot("hrp%i"%(i+1))
                if not mm:
                    mm = m[:]
                else:
                    mm = [min(mm[0],m[0]),max(mm[1],m[1])]
                valuess.append(va)
                cl.append(self.display.colors[i+1])
            text = "hits received by the player"
        elif w1 == 5:
            #bullet accuracy
            for i in range(self.display.core.nside+1):
                va,m = self.display.core.h.plot("accu%i"%(i))
                valuess.append(va)
                cl.append(self.display.colors[i])
            text = "bullet accuracy: hit/total"
            mm = [0,1]
            valuess.append([[0,1],[1,1]])
            cl.append([20,20,20])
        elif w1 == 6:
            #bullet accuracy
            for i in range(self.display.core.nside+1):
                va,m = self.display.core.h.plot("acsw%i"%(i))
                valuess.append(va)
                cl.append(self.display.colors[i])
            text = "bullet accuracy (sliding for the last 10 bullets)"
            mm = [0,1]
            valuess.append([[0,1],[1,1]])
            cl.append([20,20,20])
        #elif w1 == 7:
        #    #hit vs hit
        #    for i in range(self.display.core.nside):
        #        va,m = self.display.core.h.plot("rkh%i"%(i+1))
        #        if not mm:
        #            mm = m[:]
        #        else:
        #            mm = [min(mm[0],m[0]),max(mm[1],m[1])]
        #        valuess.append(va)
        #        cl.append(self.display.colors[i+1])
        #    text = "ratio killed-unit / unit-hit-you"
        #    valuess.append([[0,1],[1,1]])
        #    cl.append([20,20,20])

        options = {"type":"text","text":text,"size":16,"pos":[x+75,y+7]}
        self.elements.append(PGMenu(self,options))

        for i in range(len(valuess)):
            options = {"type":"plot","values":valuess[i],"extr":mm,"w":400,"h":150,"pos":[x,y+60],"cl":cl[i]}
            self.elements.append(PGMenu(self,options))

        for i in range(number):
            options = {"type":"button","text":"%i"%(i+1),"value":"plots_%i"%i,"size":40,"pos":[x+(45*i),y+220]}
            self.elements.append(PGMenu(self,options))

        options = {"type":"button","text":"back","value":"main","size":bw,"pos":[x,y+260]}
        self.elements.append(PGMenu(self,options))
        self.cursor = PGMenu(self,{"type":"cursor"})
        self.set_cursor(w1,silent=1)

    def update(self):
        """
        Update function called during the display loop
        Called by Display.update
        """
        if self.refreshed == 0:
            self.refreshed = 1
            if self.display.mode != "pause":
                if self.where == "main":
                    self.display_main()
                elif self.where in ["help","help_co","help_gp","help_bo","help_ab"]:
                    self.display_help()
                elif self.where == "achievements" or self.where.startswith("achiev"):
                    self.display_achiev()
                elif self.where.startswith("options"):
                    self.display_options()
                elif self.where == "high scores":
                    self.display_highscores()
                elif self.where == "firsttime":
                    self.display_firsttime()
            else:
                if self.where.startswith("pauseac"):
                    self.display_achiev()
                elif self.where == "high scores":
                    self.display_highscores()
                elif self.where.startswith("plots"):
                    self.display_plots()
                else:
                    self.display_pause()

        self.spgmenu.clear(self.display.screen, self.display.bkgd)

    def dirty(self):
        """
        Return the regions that need redrawing
        """
        dirty = []
        dirty.append(self.spgmenu.draw(self.display.screen))
        return dirty

    def reset(self):
        """
        Kill all the sprite and reset the variables
        """
        for l in self.elements:
            l.kill()
        self.buttons = []
        self.elements = []
        if self.cursor:
            self.cursor.kill()
        self.cursor = None
        self.refreshed = 0
        

    def click_on_button(self,i,silent=0):
        """
        When clicked on the button of index 'i' in self.buttons
        """
        if silent == 0:
            self.display.play_sound("button_cl")
        value = self.buttons[i].value
        self.reset()
        if value in ["main","high scores"] or \
           value.startswith("help") or \
           value.startswith("achiev") or \
           value.startswith("options") or \
           value.startswith("plots"):
            self.where = value
        elif value == "quit":
            self.display.core.play = 0            
        elif value == "play":
            self.display.mode = "game"
            self.display.core.new_game()
        elif value in ["pause_co","pause_ma","pause_qu"] or value.startswith("pauseac"):
            if value == "pause_co":
                #continue the game
                self.display.mode = "game"
                self.reset()
                self.display.redraw()
            elif value == "pause_ma" or value == "pause_qu":
                #abandon the game
                self.display.mode = "menu"
                if self.display.core.units[0]:
                    self.display.core.kill(self.display.core.units[0][0],silent=1)
                self.reset()
                if value == "pause_qu":
                    self.display.core.play = 0
            elif value.startswith("pauseac"):
                self.where = value
        else:
            print "TODO",value

    def check_events(self):
        """
        Check event when we are in pause or menu mode
        Called by Display.update
        """
        for event in pygame.event.get():
            if event.type == QUIT:
                self.display.core.play = 0
            elif event.type == KEYDOWN:
                if event.key == K_UP or event.key == K_z or event.key == K_w:
                    self.move_cursor(-1)
                elif event.key == K_DOWN or event.key == K_s:
                    self.move_cursor(1)
                elif event.key == K_SPACE or event.key == K_RETURN:
                    if self.cursor and self.cursor.value != -1:
                        self.click_on_button(self.cursor.value)
                elif event.key == K_ESCAPE:
                    if self.display.mode != "pause" and self.where == "main":
                        self.display.core.play = 0
                    elif self.display.mode == "pause" and self.where == "main":
                        self.click_on_button(0)
                    else:
                        self.click_on_button(-1)
            elif event.type == MOUSEBUTTONDOWN:
                mouse_rect = pygame.Rect(pygame.mouse.get_pos(),(1,1))
                i = mouse_rect.collidelist([x.rect for x in self.buttons])
                if i != -1:
                    self.click_on_button(i)
            elif event.type == MOUSEMOTION:
                mouse_rect = pygame.Rect(pygame.mouse.get_pos(),(1,1))
                i = mouse_rect.collidelist([x.rect for x in self.buttons])
                if i != -1:
                    self.set_cursor(i)

