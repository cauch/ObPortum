#! /usr/bin/env python

#    This file is part of ObPortum.
#
#    ObPortum is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ObPortum is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ObPortum.  If not, see <http://www.gnu.org/licenses/>.

class Achievements:
    def __init__(self,core):
        """
        Basic class for achievement, containing useful tools
        """
        self.core = core
        self.name = self.__class__.__name__[2:]
        self.var1 = 0     #usefull variable1
        self.var2 = 0     #usefull variable2
        self.time1 = 0    #for keeping track of the time
        self.time2 = 0    #for keeping track of the time
        #desciption of the colors of the medal image
        if "image_desc" not in dir(self):
            self.image_desc = [["empty"]]
        #create the medal image
        self.paint()

    def time(self,reset=0):
        """
        Keeping track of the time
        argument:
           reset: if =1, it reset the counter
        """
        if reset:
            self.time1 = 0
            return 0
        if self.time1 == 0:
            self.time1 = int(self.core.player_time*1./self.core.fps)
            self.time2 = int(self.core.player_time*1./self.core.fps)
        else:
            self.time2 = int(self.core.player_time*1./self.core.fps)
            return self.time2 - self.time1
        return 0

    def paint(self):
        """
        Ask Display to create a new Surface,
        stored in self.medal_images["name"],
        following the recipe given in image_desc
        """
        self.core.display.medal_paint(self.name,self.image_desc)

    def reset(self):
        """
        Reset the variables
        """
        self.var1 = 0
        self.var2 = 0
        self.time1 = 0
        self.time2 = 0

    def check(self):
        """
        Called by Core, to test if the achievement
        is achieved.
        return False if not
        return True if yes
        """
        return False

####################
# now the real achievements are here

class A_vDGZ(Achievements):
    def __init__(self,core):
        self.title = "first class"
        self.description = ["stay alive for 1 minute",""]
        self.image_desc = [
            ["01",(60,102,186)],
            ["02",(82,140,255)],
            ["03",(178,49,92)],
            ["04",(255,71,132)],
            ["05",(142,73,43)],
            ["06",(90,46,27)],
            ["07",(142,73,43)],
        ]
        Achievements.__init__(self, core)

    def check(self):
        if self.time() > 60:
            return True
        return False

class A_vDGz(Achievements):
    def __init__(self,core):
        self.title = "survivor"
        self.description = ["stay alive for 6 minutes",""]
        self.image_desc = [
            ["01",(60,102,186)],
            ["02",(82,140,255)],
            ["03",(178,49,92)],
            ["04",(255,71,132)],
            ["05",(249,229,204)],
            ["06",(178,164,146)],
            ["07",(249,229,204)],
            ["09",(249,229,204)],
        ]
        Achievements.__init__(self, core)

    def check(self):
        if self.time() > 6*60:
            return True
        return False

class A_axIl(Achievements):
    def __init__(self,core):
        self.title = "flip-flop"
        self.description = ["turn an friend into an enemy, than back to a friend",""]
        self.image_desc = [
            ["01",(173,37,37)],
            ["02",(255,55,55)],
            ["05",(193,193,193)],
            ["06",(146,146,146)],
            ["07",(193,193,193)],
            ["08",(193,193,193)],
        ]
        Achievements.__init__(self, core)

    def check(self):
        if self.var1 == 0:
            self.var1 = {}
            for k in range(1,self.core.nside+1):
                self.var1[k] = self.core.status(k)[0]
        else:
            for k in range(1,self.core.nside+1):
                ns = self.core.status(k)
                self.var1[k] += ns[0]
                while self.var1[k].count("n"):
                    self.var1[k] = self.var1[k].replace("n","")
                while self.var1[k].count("ff"):
                    self.var1[k] = self.var1[k].replace("ff","f")
                while self.var1[k].count("ee"):
                    self.var1[k] = self.var1[k].replace("ee","e")

                if self.var1[k].count("fef"):
                    return True
        return False


class A_uyTw(Achievements):
    def __init__(self,core):
        self.title = "near death experience"
        self.description = ["go below 10 health and than go back to more than 60",""]
        self.image_desc = [
            ["01",(165,173,154)],
            ["02",(207,217,193)],
            ["05",(12,11,3)],
            ["06",(105,99,33)],
            ["07",(12,11,3)],
            ["09",(12,11,3)],
        ]
        Achievements.__init__(self, core)

    def check(self):
        if not self.core.units[0]:
            return False
        if self.core.units[0][0].health < 10:
            self.var1 = 1
        if self.core.units[0][0].health > 60 and self.var1:
            return True
        return False

class A_mmLr(Achievements):
    def __init__(self,core):
        self.title = "outside of the box"
        self.description = ["stay outside of the screen for 2 minutes",""]
        self.image_desc = [
            ["01",(29,83,8)],
            ["02",(45,125,13)],
            ["03",(74,36,15)],
            ["04",(125,61,26)],
            ["06",(125,125,125)],
            ["07",(165,165,120)],
        ]
        Achievements.__init__(self, core)

    def check(self):
        if not self.core.units[0]:
            return False
        b = 0
        if self.core.units[0][0].pos[0] < 0:
            b = 1
        if self.core.units[0][0].pos[1] < 0:
            b = 1
        if self.core.units[0][0].pos[0] > self.core.display.screen_width:
            b = 1
        if self.core.units[0][0].pos[1] > self.core.display.screen_height:
            b = 1
        if not b:
            self.time(reset=1)
        if b:
            if self.time() > 60*2:
                return True
        return False


class A_Qopo(Achievements):
    def __init__(self,core):
        self.title = "avoiding the bullet"
        self.description = ["avoid being hit for 1 minute",""]
        self.image_desc = [
            ["01",(64,125,111)],
            ["02",(93,182,161)],
            ["05",(182,125,75)],
            ["06",(143,98,59)],
            ["07",(182,125,75)],
        ]
        Achievements.__init__(self, core)

    def check(self):
        if not self.core.units[0]:
            return False
        b = 1
        if self.var1 > self.core.units[0][0].health:
            b = 0
        self.var1 = self.core.units[0][0].health
        if not b:
            self.time(reset=1)
        if b:
            if self.time() > 60:
                return True
        return False

class A_QoPo(Achievements):
    def __init__(self,core):
        self.title = "dodge master"
        self.description = ["avoid being hit for 2 minutes",""]
        self.image_desc = [
            ["01",(64,125,111)],
            ["02",(93,182,161)],
            ["05",(249,229,204)],
            ["06",(178,164,146)],
            ["07",(249,229,204)],
            ["09",(249,229,204)],
        ]
        Achievements.__init__(self, core)

    def check(self):
        if not self.core.units[0]:
            return False
        b = 1
        if self.var1 > self.core.units[0][0].health:
            b = 0
        self.var1 = self.core.units[0][0].health
        if not b:
            self.time(reset=1)
        if b:
            if self.time() > 2*60:
                return True
        return False

class A_iCcC(Achievements):
    def __init__(self,core):
        self.title = "love and peace"
        self.description = ["be friend with everybody",""]
        self.image_desc = [
            ["01",(206,206,168)],
            ["02",(255,255,209)],
            ["03",(80,80,60)],
            ["04",(129,129,109)],
            ["06",(217,217,217)],
            ["07",(181,151,5)],
            ["08",(181,151,5)],
        ] 
        Achievements.__init__(self, core)

    def check(self):
        for k in range(self.core.nside):
            if self.core.status(k+1) != "friend":
                return False
        return True

class A_ghSe(Achievements):
    def __init__(self,core):
        self.title = "hate and war"
        self.description = ["be enemy to everybody",""]
        self.image_desc = [
            ["01",(206,206,168)],
            ["02",(255,255,209)],
            ["03",(80,60,60)],
            ["04",(129,109,109)],
            ["06",(217,217,217)],
            ["07",(181,5,17)],
            ["08",(181,5,17)],
        ]
        Achievements.__init__(self, core)

    def check(self):
        for k in range(self.core.nside):
            if self.core.status(k+1) != "enemy":
                return False
        return True

class A_qxgD(Achievements):
    def __init__(self,core):
        self.title = "asteroids"
        self.description = ["don't move for 30 seconds",""]
        self.image_desc = [
            ["01",(117,7,181)],
            ["02",(155,10,239)],
            ["05",(239,217,74)],
            ["06",(239,156,31)],
            ["07",(239,217,74)],
        ]
        Achievements.__init__(self, core)

    def check(self):
        if not self.core.units[0]:
            return False
        if self.var1 == 0:
            self.var1 = self.core.units[0][0].pos[:]
        b = 1
        if self.var1[0] != self.core.units[0][0].pos[0]:
            b = 0
        if self.var1[1] != self.core.units[0][0].pos[1]:
            b = 0
        if not b:
            self.time(reset=1)
            self.var1 = self.core.units[0][0].pos[:]
        if b:
            if self.time() > 30:
                return True
        return False

class A_dSjo(Achievements):
    def __init__(self,core):
        self.title = "gallery shooter"
        self.description = ["kill in a row 25 units with a bullet traveling from left",
                            " to right"]
        self.image_desc = [
            ["01",(83,132,24)],
            ["02",(117,185,34)],
            ["05",(81,80,93)],
            ["06",(45,44,52)],
            ["07",(81,80,93)],
            ["08",(81,80,93)],
        ]
        Achievements.__init__(self, core)
        #this is a good example of intercepting info by hacking
        #into the interesting function
        self.original = self.core.hit
        self.core.hit = self.modified_hit

    def modified_hit(self,hit_unit,bullet):
        self.original(hit_unit,bullet)
        if bullet.side != hit_unit.side and bullet.side == 0:
            if bullet.direction[0] <= 0:
                self.var1 = 0
            else:
                self.var1 += 1

    def check(self):
        if self.var1 >= 25:
            return True
        return False

class A_ozXu(Achievements):
    def __init__(self,core):
        self.title = "pacifist"
        self.description = ["don't shoot anybody for 2 minutes",""]
        self.image_desc = [
            ["01",(190,57,50)],
            ["02",(255,77,68)],
            ["06",(255,224,122)],
            ["09",(208,182,94)],
        ]
        Achievements.__init__(self, core)
        self.original = self.core.hit
        self.core.hit = self.modified_hit

    def modified_hit(self,hit_unit,bullet):
        self.original(hit_unit,bullet)
        if bullet.side != hit_unit.side and bullet.side == 0:
            self.time(1)

    def check(self):
        if self.time() > 2*60:
            return True
        return False

class A_yYuy(Achievements):
    def __init__(self,core):
        self.title = "lesser of two evils"
        self.description = ["kill in a row 10 units that belongs to the team",
                            " that is in minority number"]
        self.image_desc = [
            ["01",(83,149,180)],
            ["02",(141,176,192)],
            ["05",(253,227,44)],
            ["06",(171,154,29)],
            ["07",(253,227,44)],
        ]
        Achievements.__init__(self, core)
        self.original = self.core.hit
        self.core.hit = self.modified_hit

    def modified_hit(self,hit_unit,bullet):
        self.original(hit_unit,bullet)
        if bullet.side != hit_unit.side and bullet.side == 0:
            nH = len(self.core.units[hit_unit.side])
            ns = [len(self.core.units[k]) for k in self.core.units.keys() if k != 0]
            if nH > min(ns):
                self.var1 = 0
            else:
                self.var1 += 1

    def check(self):
        if self.var1 >= 10:
            return True
        return False

class A_gsdd(Achievements):
    def __init__(self,core):
        self.title = "over-population"
        self.description = ["get one team to more than 40 units",
                            ""]
        self.image_desc = [
            ["01",(195,195,195)],
            ["02",(230,230,230)],
            ["03",(61,55,200)],
            ["04",(104,100,210)],
            ["05",(255,10,25)],
            ["06",(255,10,25)],
            ["07",(230,230,230)],
            ["09",(230,230,230)],
        ]
        Achievements.__init__(self, core)

    def check(self):
        ns = [len(self.core.units[k]) for k in self.core.units.keys() if k != 0]
        if max(ns) > 40:
            return True
        return False

class A_aapl(Achievements):
    def __init__(self,core):
        self.title = "select committee"
        self.description = ["maintain the population below 10 for 3 minutes",
                            ""]
        self.image_desc = [
            ["01",(64,122,62)],
            ["02",(98,186,95)],
            ["05",(140,170,176)],
            ["06",(74,82,89)],
            ["07",(140,170,176)],
        ]
        Achievements.__init__(self, core)

    def check(self):
        ns = [len(self.core.units[k]) for k in self.core.units.keys() if k != 0]
        if sum(ns) > 10:
            self.time(1)
        if self.time() > 3*60:
            return True
        return False

class A_mfsS(Achievements):
    def __init__(self,core):
        self.title = "consumer society"
        self.description = ["take all the bonuses that appears in a row",
                            "for 12 bonuses"]
        self.image_desc = [
            ["01",(211,95,95)],
            ["02",(222,135,135)],
            ["03",(171,55,200)],
            ["04",(188,95,211)],
            ["05",(255,177,10)],
            ["06",(255,177,10)],
            ["07",(230,230,230)],
            ["09",(230,230,230)],
        ]
        Achievements.__init__(self, core)
        self.original_kb = self.core.kill_bonus
        self.core.kill_bonus = self.modified_kb
        self.original_gb = self.core.get_bonus
        self.core.get_bonus = self.modified_gb

    def modified_kb(self,bonus):
        self.original_kb(bonus)
        if self.var2 == 0:
            self.var1 = 0
        self.var2 = 0

    def modified_gb(self,bonus):
        self.original_gb(bonus)
        self.var1 += 1
        self.var2 = 1

    def check(self):
        if self.var1 >= 12:
            return True
        return False

class A_desO(Achievements):
    def __init__(self,core):
        self.title = "precision shooter"
        self.description = ["don't miss 5 shots in a row",
                            ""]
        self.image_desc = [
            ["01",(21,95,95)],
            ["02",(30,135,135)],
            ["05",(201,211,220)],
            ["06",(38,40,42)],
            ["07",(201,211,220)],
            ["08",(201,211,220)],
        ]
        Achievements.__init__(self, core)
        self.original_h = self.core.hit
        self.core.hit = self.modified_hit
        self.original_k = self.core.kill
        self.core.kill = self.modified_kill

    def modified_kill(self,unit,silent=0):
        if unit.what == 1 and unit.side == 0:
            if self.var1:
                self.var2 += 1
            else:
                self.var2 = 0
        self.original_k(unit,silent)

    def modified_hit(self,hit_unit,bullet):
        if bullet.side != hit_unit.side and bullet.side == 0:
            self.var1 = 1
        self.original_h(hit_unit,bullet)
        self.var1 = 0

    def check(self):
        if self.var2 >= 5:
            return True
        return False

class A_qsXe(Achievements):
    def __init__(self,core):
        self.title = "true hatred"
        self.description = ["go down to -20 enemy level",
                            ""]
        self.image_desc = [
            ["01",(170,162,87)],
            ["02",(212,202,108)],
            ["03",(43,41,22)],
            ["04",(78,74,39)],
            ["06",(219,157,12)],
            ["07",(189,127,12)],
        ]
        Achievements.__init__(self, core)

    def check(self):
        for k in range(self.core.nside):
            if self.core.ratio[k+1] < -20:
                return True
        return False

class A_fwWz(Achievements):
    def __init__(self,core):
        self.title = "killer"
        self.description = ["kill more than 80 enemies",
                            ""]
        self.image_desc = [
            ["01",(91,50,78)],
            ["02",(139,76,119)],
            ["05",(18,21,24)],
            ["06",(18,21,24)],
            ["07",(35,40,45)],
        ]
        Achievements.__init__(self, core)
        self.original = self.core.hit
        self.core.hit = self.modified_hit

    def modified_hit(self,hit_unit,bullet):
        self.original(hit_unit,bullet)
        if bullet.side != hit_unit.side and bullet.side == 0:
            self.var1 += 1

    def check(self):
        if self.var1 >= 80:
            return True
        return False

class A_hGfd(Achievements):
    def __init__(self,core):
        self.title = "long range"
        self.description = ["kill 5 enemies in a row which are more than 3/4",
                            "the screen-width away from you"]
        self.image_desc = [
            ["01",(143,190,184)],
            ["02",(167,221,214)],
            ["03",(67,75,82)],
            ["04",(101,113,123)],
            ["05",(221,157,37)],
            ["06",(170,121,28)],
            ["07",(221,147,37)],
            ["08",(221,147,37)],
        ]
        Achievements.__init__(self, core)
        self.original = self.core.hit
        self.core.hit = self.modified_hit

    def modified_hit(self,hit_unit,bullet):
        if bullet.side != hit_unit.side and bullet.side == 0:
            if self.core.units[0]:
                dx = self.core.units[0][0].pos[0]-hit_unit.pos[0]
                dy = self.core.units[0][0].pos[1]-hit_unit.pos[1]
                if dx**2 + dy**2 >= (self.core.display.screen_width*0.75)**2:
                    self.var1 += 1
                else:
                    self.var1 = 0
        self.original(hit_unit,bullet)

    def check(self):
        if self.var1 >= 5:
            return True
        return False



